Script renames all files in current dir and add them unique ID

### Requirements

Python 3

## Getting Started

1. Put main.py into directory with files you want to rename

2. Double click on main.py

3. Remove main.py from dir

## Authors

* Leo Nardo

See also the list of [contributors](http://192.168.88.240:3000/leonardo/script.gen_uuid) who participated in this project.
